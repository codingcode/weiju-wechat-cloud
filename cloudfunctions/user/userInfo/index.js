//更新用户信息
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database() //数据库
const userInfo = async function (params) {
  const wxContext = cloud.getWXContext()

  let openid = params.openid || wxContext.OPENID

  const infoResult = await db.collection('user').where({
    _openid: openid
  }).get()
  //动态部分
  const dynamicEndorseResult = await db.collection('dynamicEndorse').where({
    _openid: openid
  }).count()
  const dynamicResult = await db.collection('dynamic').where({
    _openid: openid
  }).count()
  //诉求部分
  const appealEndorseResult = await db.collection('appealEndorse').where({
    _openid: openid
  }).count()
  const appealResult = await db.collection('appeal').where({
    _openid: openid
  }).count()
  if (infoResult.data) {
    let userData = infoResult.data[0]
    userData.dynamicCount = dynamicResult.total
    userData.endorseCount = dynamicEndorseResult.total + appealEndorseResult.total
    userData.appealCount = appealResult.total
    return userData
  }
}
module.exports = userInfo